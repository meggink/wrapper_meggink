package nl.bioinf.meggink.wrapper_meggink;

public interface OptionsProvider {
    String getFileName();
}
