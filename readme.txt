title: Java Wrapper
author: Marijke Eggink
date: 16-11-2020

This project contains a java wrapper that can predict if a bcell has antibody inducing activity.

In the data directory there is a unkown_bcell.arff file, here are 3 unknown instances.
When you run the main of WekaRunner.java these instances will be classified. Where true means they have antibody inducing acitivity and false means they don't.